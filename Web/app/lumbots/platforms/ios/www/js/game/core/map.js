// Filename: app.js
define([
    'jquery',
    'd3',
    'topojson'
], function ($, d3, topojson) {
    return {
        id: null,
        width: null,
        height: null,
        projection: null,
        svg: null,
        center: [-121.960197 , 37.350810],
        scale: 4000,
        precision: 10,

        init: function () {
            Game.log("Initialize Map D3 Version: " + d3.version);
            Game.log("Initialize Topjson Version: " + topojson.version);
            this.setDimentions();
        },

        setDimentions: function () {
            Game.log("Map: setDimentions");
            var tmpHeight = Game.height - 50;
            var tmpWidth = Game.width;

            this.width = tmpWidth;
            this.height = tmpHeight;
        },

        changeViewPort: function () {

        },

        getProjection: function () {
            return this.projection;
        },

        moveTo: function (lat, long) {
            this.projection.center([lat, long]);
        },

        draw: function () {
            Game.log("draw Map");
            var width = this.width,
                height = this.height;

            this.projection = d3.geo.mercator()
                .center(this.center)
                .scale(this.scale)
                .translate([width / 2, height / 2])
                .clipExtent([
                    [0, 0],
                    [width, height]
                ])
                .precision(this.precision);

            var path = d3.geo.path()
                .projection(this.projection);

            var graticule = d3.geo.graticule()
                .step([5, 5]);


            this.svg = Game.svg.insert("g", "#gamecanvas")
                .attr("id", "gamemap");

            this.svg.append("path")
                .datum(graticule)
                .attr("class", "graticule")
                .attr("height", height)
                .attr("width", width)
                .attr("d", path);


            // svg.append("circle")
            //     .attr("class", "dot")
            //     .attr("transform", "translate(" + this.projection([-66, 18]) + ")")
            //     .attr("r", 8);
            d3.json("/js/game/data/world.json", function (error, world) {
                Game.map.svg.insert("path", ".graticule")
                    .datum(topojson.feature(world, world.objects.land))
                    .attr("class", "land")
                    .attr("d", path);
            });

            d3.select(self.frameElement).style("height", height + "px");
        }
    };
});