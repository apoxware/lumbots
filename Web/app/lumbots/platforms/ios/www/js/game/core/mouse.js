// Filename: app.js
define([
    'd3'
], function (d3) {
    return {

        x: 0,
        y: 0,
        init: function () {
            Game.log("Initialize Game Input");
            this.setEventHandlers();
        },

        setEventHandlers: function () {
            Game.log("setEventListeners");
            Game.svg.on("mousedown", $.proxy(function () {
                Game.log("mouse down");
                this.onCursor("down", event)
            }, this));

            Game.svg.on("mouseup", $.proxy(function () {
                Game.log("mouse up");
                this.onCursor("up", event);
                this.eventHandler(event);
            }, this));

            Game.svg.on("touchstart", $.proxy(function () {
                Game.log("touch start");
                this.onCursor("down", event.changedTouches[0])
            }, this));

            Game.svg.on("touchend", $.proxy(function () {
                Game.log("touch end");
                this.onCursor("up", event);
                this.eventHandler(event.changedTouches[0]);
            }, this));
        },

        eventHandler: function (event) {
            Game.log("eventX: " + event.pageX + " eventY: " + event.pageY);
            var clickX = event.pageX;
            var clickY = event.pageY - Game.headerHeight;
            Game.log("Colnvert to Long Lat");
            var touchPoint = Game.map.projection.invert([clickX , clickY]);
            Game.log("event Lat: " + touchPoint[0] + " eventLong: " + touchPoint[1]);
            Game.checkEntitySelection(touchPoint[0], touchPoint[1]);
        },

        onCursor: function (action, event) {
            switch (action) {
                case "down":
                    this.cursorDown(event);
                    break;
                case "up":
                    this.cursorUp(event);
                    break;
                case "move":
                    this.cursorMove(event);
                    break;
                default:
                    Game.log("unhandled mouse action");
            }
        },

        setEventWaiters: function () {
            var deferred = $.Deferred();


            Game.log("setEventWaiters");
            Game.svg.on("mousedown", $.proxy(function () {
                Game.log("mouse down");
            }, this));

            Game.svg.on("mouseup", $.proxy(function () {
                Game.log("mouse up");
                deferred.resolve(event);
            }, this));

            Game.svg.on("touchstart", $.proxy(function () {
                Game.log("touch start");
            }, this));

            Game.svg.on("touchend", $.proxy(function () {
                Game.log("touch end");
                deferred.resolve(event.changedTouches[0]);
            }, this));

            return deferred.promise().then(function (event) {
                Game.input.setEventHandlers()
            });
        },

        eventWaiter: function (event) {

            var action = this.selectedAction.action;
            var actionCost = (this.selectedAction.cost) ? this.selectedAction.cost : this.getActionCost(this.selectedAction.action);
            Game.log("eventX: " + event.pageX + " eventY: " + event.pageY);
            var clickX = event.pageX;
            var clickY = event.pageY - this.headerHeight;
            Game.log("Convert to Long Lat");
            var touchPoint = Game.map.projection.invert([clickX , clickY]);
            Game.log("event Lat: " + touchPoint[0] + " eventLong: " + touchPoint[1]);
            var beacon = this.selectedBeacon;

            if (beacon.isValidCreateLocation(touchPoint[0], touchPoint[1])) {
                switch (action) {
                    case "moveLumbots":
                        this.player.energy -= actionCost;
                        beacon.moveChildLumbots(touchPoint[0], touchPoint[1]);
                        this.hideMessage();
                        break;
                    case "createBeacon":
                        beacon.createBeacon(touchPoint[0], touchPoint[1]);
                        break;
                    default:
                        Game.log("unhandled action");
                }
            } else {
                this.showTimedMessage("This is not a valid location. Please select again your option");
            }
            this.hideMenu();
            this.selectedBeacon.isSelected = false;
            this.selectedBeacon = null;
            this.selectedAction = null;

        },

        cursorMove: function (event) {
            var mapPoint = Game.map.projection.invert([event.pageX , event.pageY - Game.headerHeight]);
            var cursorLat = mapPoint[0];
            var cursorLong = mapPoint[1];
            d3.selectAll(".cursor")
                .attr("transform", "translate(" + Game.map.projection([cursorLat, cursorLong]) + ")")
                .transition();
        },

        cursorDown: function (event) {
            var mapPoint = Game.map.projection.invert([event.pageX , event.pageY - Game.headerHeight]);
            var cursorLat = mapPoint[0];
            var cursorLong = mapPoint[1];
            Game.svg.append("circle")
                .attr("class", "cursor")
                .attr("r", 0)
                .attr("transform", "translate(" + Game.map.projection([cursorLat, cursorLong]) + ")")
                .transition()
                .attr("r", 20);
        },

        cursorUp: function (event) {
            d3.selectAll(".cursor")
                .transition()
                .attr("r", 40)
                .style("opacity", 0)
                .transition()
                .remove();
        },

        draw: function () {
        }

    };
});