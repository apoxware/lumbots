define("game/core/utilities", [], function () {
    return {
        count: 0,
        hasElapsed: function (ratio, mod) {

            this.count += ratio * mod;
            return (this.count >= 1);
        },

        showGainXPAnimation: function (xp) {
            Game.log("Animating Player XP Gained");
            $('#NotificationsAnimation').empty();
            $('#NotificationsAnimation').html("+ " + xp);
            $('#NotificationsAnimation').css("display", "inline-block");
            $('#NotificationsAnimation').animate({
                opacity: 1.0,
                fontSize: "25px"
            }, 300, function () {
                $('#NotificationsAnimation').delay(300).animate({
                    opacity: 0.0,
                    fontSize: "30px"
                }, 300, function () {
                    setTimeout(function () {
                        $('#NotificationsAnimation').css("font-size", "20px");
                        $('#NotificationsAnimation').css("opacity", 0);
                    }, 500);
                });
            });
        },

        hideEntityDisplay: function () {
            $('#actionslist').fadeOut();
            $('#avatarstats').fadeOut();
        },

        validateAction: function (action) {
            Game.log("Validating action & energy transaction");
        },

        appendData: function (obj, data) {
            for (var k in data) {
                obj[k] = data[k];
            }
            Game.log("Appending Data to Object");
        },
        getModulePaths: function (modules) {
            var modules = $.extend(true, [], modules);
            var allMods = [];

            for (var k in modules) {
                var req = modules[k];
                var reqPath = req.classPath;
                allMods.push(reqPath);
            }
            return allMods;
        },

        loadOptionsForEntity: function (entity) {
            $('#actionslist').fadeOut(500, $.proxy(function () {
                $('#actionslist').empty();
                this.updateOptionsForEntity(entity);
                $('#actionslist').fadeIn();
            }, this));
        },

        updateOptionsForEntity: function (entity) {
            $('#actionslist').empty();
            for (var i = 0; i < entity.options.length; i++) {
                var option = entity.options[i];
                if (option.requiredLvl <= Game.player.level) {
                    var name = option.name;
                    var cost = (option.cost) ? option.cost : 0;
                    var listItem = $('<li>', {});
                    var listText = $('<div>', {"class": "itemAction", text: name});
                    var listCost = $('<div>', {"class": "itemEnergy", text: cost});
                    listItem.append(listText);
                    listItem.append(listCost);
                    //listItem.click(function(){console.log("wtf")});
                    listItem.click({entity: entity, option: option}, $.proxy(Game.menuActionClicked, this));
                    $('#actionslist').append(listItem);
                }
            }
        },

        getModuleByType: function (type) {
            var modules = $.extend(true, [], Game.modules);
            var mod = null;

            for (var k in modules) {
                var req = modules[k];
                if (req.childType === type) {
                    mod = req.module;
                }
            }
            return mod;
        },

        getAllEntitiesByType: function (player, type) {
            var entities = [];
            for (var k in player.entities) {
                var pEntity = player.entities[k];
                if (pEntity.type === type) {
                    entities.push(pEntity);
                }
            }
            return entities;
        },

        /*
         * Retreives an element by its uid from the entity list.
         */
        getEntityByUID: function (player, uid) {
            var element = null;

            for (var k in player.entities) {
                if (player.entities[k].uid === uid) {
                    element = player.entities[k];
                }
            }
            return element;

        },

        /*
         * Retreives the specific hp and ap for the lumbots depending on player level.
         */
        getLumbotProperties: function (player) {
            var props = $.extend(true, {}, Game.entityprops["lumbot"].levels[player.level - 1]);
            var equippedItem = this.getPlayerEquippedItem(player);
            if (equippedItem) {
                var item = $.extend(true, {}, equippedItem);
                props.ap += item.ap;
                props.hp += item.hp;
            }
            return props;
        },

        getPlayerEquippedItem: function (player) {
            var playerItems = player.items;
            var equippedItem = null;
            if (playerItems) {
                for (var i = 0; i < playerItems.length; i++) {
                    var item = playerItems[i];
                    if (item.equipped) {
                        equippedItem = item;
                        break;
                    }
                }
            }
            return equippedItem;
        },

        generateUID: function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    }
});