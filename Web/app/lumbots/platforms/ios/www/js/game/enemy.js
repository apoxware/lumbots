define("game/enemy", [], function () {
    return {
        level: 1,
        id: null,
        maxEnergy: 0,
        maxBeacons: 0,
        clazz: "enemy",
        levelCap: 0,
        objects: [],
        energy: 0,
        timeToAttack: 10000, //milliseconds to each move
        attackInterval: null,
        lookUpRadius: 30,

        init: function () {
            Game.log("Game Player Init");

            //Set Beacon Properties by level
            var playerProps = Game.entityprops["player"].levels[this.level - 1];
            Game.util.appendData(this, playerProps);

            this.attackInterval = setInterval(
                $.proxy(this.doRandomAttack, this), this.timeToAttack
            );

        },
        gainXP: function (xp) {
            this.levelXP += xp;
        },

        gainEnergy: function (energy) {
            this.energy += energy;
        },

        removeEnergy: function (energy) {
            this.energy -= energy;
        },

        levelUp: function () {
            this.level++;
            this.init();
            //Increase lookup radius?
            //Increase attack interval?

            Game.showTimedMessage("Congratulations! You have leveled up!");
            //Game.showTimedMessage("Player has gained up!");
        },
        checkLevelXP: function () {
            if (this.levelXP >= this.levelCap) {
                this.levelUp();
            }
        },

        handleTransaction: function (cost) {
            this.energy -= cost;

        },

        createRandomBeacon: function () {

        },

        lookForNextRandomTarget: function () {

        },

        /*
         * TODO: Random Attack timeToAttack should be set randomly after each attack? from a range? Range should be
         * change per level?
         * */
        doRandomAttack: function () {
            Game.log("doRandomAttack");
            /*
             * 1) Go thru all beacons
             * 2) Check from center of beacon to this.lookupradius for other targets
             * 3) Try creating a closer beacon to the area
             * 4) If area is closer than some other radius try to attack
             * 5) attacking should have some cost? maybe its not something we do unless we are certain.
             * 6) Continue Expanding? Continue Leveling?
             * 7) Set Walls? Set Turrets in tactic positions
             * */

        },
        initEntities: function (callback) {
            var entities = $.extend(true, [], this.entities);
            this.entities = [];
            //Init each entity with corresponding type
            for (var e in entities) {
                var entity = entities[e];
                var entityModule = Game.util.getModuleByType(entity.type);
                Game.util.appendData(entityModule, entity);
                entityModule.isAI = true;
                entityModule.uid = Game.util.generateUID();
                this.entities.push(entityModule);
                entityModule.init();
            }
            callback();
        }

    };
});