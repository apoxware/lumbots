define("game/entities/entityprops", [ //This would only store data that would come from the backend

], function () {
    return {
        player: {
            levels: [
                {
                    maxEnergy: 60,
                    beacon: 5,
                    turret: 1,
                    levelCap: 50
                },
                {
                    maxEnergy: 100,
                    beacon: 6,
                    turret: 2,
                    levelCap: 100
                },
                {
                    maxEnergy: 150,
                    beacon: 7,
                    turret: 3,
                    levelCap: 80
                },
                {
                    maxEnergy: 200,
                    beacon: 8,
                    turret: 3,
                    levelCap: 500
                },
                {
                    maxEnergy: 200,
                    beacon: 8,
                    turret: 3,
                    levelCap: 1000
                }
            ]
        },
        turret: {
            options: [
                {
                    name: "Delete Turret",
                    description: "Delete current turret",
                    action: "deleteturret",
                    cost: 15,
                    requiredLvl: 0
                }
            ],
            levels: [
                {
                    cost: 25,
                    radius: 10,
                    attackRatio: 0.70,
                    outterRadius: 80,
                    hp: 100,
                    ap: 100
                },
                {
                    cost: 35,
                    radius: 10,
                    attackRatio: 0.80,
                    outterRadius: 100,
                    hp: 110,
                    ap: 110
                },
                {
                    cost: 45,
                    radius: 10,
                    attackRatio: 0.90,
                    outterRadius: 110,
                    hp: 120,
                    ap: 120
                }
            ]
        },
        beacon: {
            options: [
                {
                    name: "Create Beacon",
                    description: "Please select an area where you want your new beacon",
                    action: "createBeacon",
                    cost: 25,
                    requiredLvl: 0

                },
                {
                    name: "Move Lumbots",
                    description: "Please select an area where you want to move your lumbots",
                    action: "moveLumbots",
                    cost: 1,
                    requiredLvl: 0
                },
                {
                    name: "Delete Beacon",
                    description: "Delete current beacon",
                    action: "deletebeacon",
                    requiredLvl: 0,
                    cost: 15
                },
                {
                    name: "Create Turret",
                    description: "Please select an area where you want to create your turret",
                    action: "createturret",
                    requiredLvl: 3,
                    cost: 100
                },
                {
                    name: "Create Dim Shield",
                    description: "Please select an area where you want to create your dim shield",
                    action: "createdim",
                    requiredLvl: 4,
                    cost: 100
                }
            ],
            levels: [
                {
                    cost: 25,
                    radius: 10,
                    maxLumbots: 3,
                    spawnRatio: 0.09,
                    energyRatio: 0.70,
                    outterRadius: 135,
                    hp: 100
                },
                {
                    cost: 50,
                    radius: 14,
                    maxLumbots: 5,
                    spawnRatio: 0.06,
                    energyRatio: 0.20,
                    outterRadius: 145,
                    hp: 120
                },
                {
                    cost: 90,
                    radius: 16,
                    maxLumbots: 8,
                    spawnRatio: 0.06,
                    energyRatio: 0.20,
                    outterRadius: 155,
                    hp: 160
                },
                {
                    cost: 150,
                    radius: 18,
                    maxLumbots: 10,
                    spawnRatio: 0.06,
                    energyRatio: 0.20,
                    outterRadius: 165,
                    hp: 190
                }
            ]
        },
        xp: {
            destroyBeacon: 15,
            destroyTurret: 15,
            createBeacon: 5,
            createTurret: 10,
            upgradeBeacon: 5,
            upgradeTurret: 5
        },
        lumbot: {
            levels: [
                {
                    hp: 10,
                    ap: 10
                },
                {
                    hp: 12,
                    ap: 12
                },
                {
                    hp: 14,
                    ap: 14
                },
                {
                    hp: 15,
                    ap: 15
                },
                {
                    hp: 16,
                    ap: 16
                },
                {
                    hp: 17,
                    ap: 17
                },
                {
                    hp: 30,
                    ap: 30
                }
            ]
        }
    };
});