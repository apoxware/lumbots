// Filename: app.js
define("game/entities/lumbot", [
    'jquery',
    'd3',
], function ($, d3) {
    return {
        id: null,
        level: 1,
        hp: 20,
        ap: 20,
        radius: 1,
        outterRadius: 45,
        lat: null,
        long: null,
        targetlat: null,
        targetlong: null,
        state: "idle",  //idle //moving
        speed: 0.9,
        attackSpeed: 0.8,
        attackRadius: 4, //sum to current radius
        orientation: 0,
        isAI: false,
        turnMod: 15,
        drawOuter: false,
        parentBeaconUId: null,
        attCount: 0,
        idleTimer: 0,
        idleMaxTime: 5,
        canBeSelected: false,
        type: "lumbot",

        init: function () {
            Game.log("Init Lumbot");
            this.myParentPlayer = (!this.isAI) ? Game.player : Game.enemy;
            var lumbotProps = Game.util.getLumbotProperties(this.myParentPlayer);
            Game.util.appendData(this, lumbotProps);
            this.initAnim();
        },

        initAnim: function () {
            Game.svg.append("circle")
                .attr("class", "lumbotAnim")
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius * 25)
                .style("opacity", 0)
                .transition()
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius)
                .style("opacity", 1)
                .remove();
        },

        attackAnim: function () {
            Game.svg.append("circle")
                .attr("class", "lumbotAnim")
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius * 25)
                .style("opacity", 0)
                .transition()
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius)
                .style("opacity", 1)
                .remove();
        },

        update: function (mod) {
            //hero.x -= hero.speed * modifier;

            //Get X and Y Position
            var position = Game.map.projection([this.lat, this.long]);
            var posX = position[0];
            var posY = position[1];
            var turnSpeed = Game.math.circle / this.turnMod;

            //target position
            var targetPosition = Game.map.projection([this.targetlat, this.targetlong]);
            var tarPosX = targetPosition[0];
            var tarPosY = targetPosition[1];

            //Check if lumbot got to its destinations. If it did set to idle
            if (Game.math.intercept(tarPosX, tarPosY, posX, posY, this.radius + this.attackRadius)) {
                this.state = "idle";
            }

            //determine how close we are from next target
            var y = tarPosY - posY;
            var x = tarPosX - posX;
            var d2 = Math.pow(x, 2) + Math.pow(y, 2);

            //calculate orientation we want to move
            var angle = Math.atan2(y, x);
            var delta = angle - this.orientation;
            var delta_abs = Math.abs(delta);

            // if the different is more than 180°, convert
            // the angle a corresponding negative value
            if (delta_abs > Math.PI) {
                delta = delta_abs - Game.math.circle;
            }

            // if the angle is already correct,
            // don't bother adjusting
            if (delta !== 0) {
                // do we turn left or right?
                var direction = delta / delta_abs;
                // update our orientation
                this.orientation += (direction * Math.min(turnSpeed, delta_abs));
            }
            // constrain orientation to reasonable bounds
            this.orientation %= Game.math.circle;

            // use orientation and speed to update our position
            posX += Math.cos(this.orientation) * this.speed;
            posY += Math.sin(this.orientation) * this.speed;

            //Convert to lat and long project on map
            var newMapPos = Game.map.projection.invert([posX, posY]);
            this.lat = newMapPos[0];
            this.long = newMapPos[1];

            //check for enemy collision
            this.checkCollision(mod);

            //check for item collision
            this.checkItemCollision(mod);

            //check health
            if (this.hp <= 0) {
                this.remove();
            }

            //check state if idle else reset timer;
            if (this.state == "idle") {
                this.idleTimer += mod;
            } else {
                this.idleTimer = 0;
            }

            //if idletimer reaches idleMaxTime seconds then return to base
            if (this.idleTimer >= this.idleMaxTime) {
                this.moveToParentBeacon();
            }

            this.updateLumbotProperties();

        },

        updateLumbotProperties: function () {
            var lumbotProps = Game.util.getLumbotProperties(this.myParentPlayer);
            this.ap = lumbotProps.ap;
            this.hp = lumbotProps.hp;
        },

        checkItemCollision: function (mod) {
            var myPosition = Game.map.projection([this.lat, this.long]);
            var items = Game.itemsctrl.getItems();
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var targetPos = Game.map.projection([item.lat, item.long]);
                var isWithinInnerRange = Game.math.intercept(targetPos[0], targetPos[1], myPosition[0], myPosition[1], this.radius);

                //check if target is within range. Then Attack
                if (isWithinInnerRange) {
                    Game.itemsctrl.itemAquiredBy(item, this.myParentPlayer);
                }
            }
        },

        checkCollision: function (mod) {
            //Create pool of all possible targets. TODO: maybe create pool of only closest to this specific lumbot?
            var myPosition = Game.map.projection([this.lat, this.long]);
            var myParentEnemy = (this.isAI) ? Game.player : Game.enemy;
            for (var i = 0; i < myParentEnemy.entities.length; i++) {
                var entity = myParentEnemy.entities[i];
                var targetPos = Game.map.projection([entity.lat, entity.long]);
                var isWithinOuterRange = Game.math.intercept(targetPos[0], targetPos[1], myPosition[0], myPosition[1], this.outterRadius);
                var isWithinInnerRange = Game.math.intercept(targetPos[0], targetPos[1], myPosition[0], myPosition[1], this.radius + this.attackRadius);

                //check if is withioutter range
                if (isWithinOuterRange) {
                    this.state = "attacking";
                    this.moveTo(entity.lat, entity.long);
                }

                //check if target is within range. Then Attack
                if (isWithinInnerRange) {
                    this.attCount += this.attackSpeed * mod;
                    if (this.attCount >= 1) {
                        this.attack(entity);
                        this.attCount = 0;
                    }
                }
            }
        },

        moveToParentBeacon: function () {
            try {
                var myBeacon = Game.util.getEntityByUID(this.myParentPlayer, this.parentBeaconUId);
                this.moveTo(myBeacon.lat, myBeacon.long);
            } catch (e) {
                Game.log("Error moivng lumbot to parent. Deleting lumbot" + e);
                this.remove();
            }

        },

        remove: function () {
            var myBeacon = Game.util.getEntityByUID(this.myParentPlayer, this.parentBeaconUId);
            var index = myBeacon.lumbots.indexOf(this);
            myBeacon.lumbots.splice(index, 1);
            var entityIndex = this.myParentPlayer.entities.indexOf(this);
            this.myParentPlayer.entities.splice(entityIndex, 1);
            delete this;
        },


        moveTo: function (lat, long) {
            this.state = "moving";
            this.targetlat = lat;
            this.targetlong = long;
        },

        attack: function (enemy) {


            this.attackAnim();
            var enemyHP = enemy.hp;
            var enemyAP = enemy.ap;

            if (enemyAP) {
                this.hp -= enemyAP;
            }
            enemy.hp -= this.ap;

        },

        draw: function () {
            //Game.log("Update Lumbot");
            var lumbotClass = (!this.isAI) ? "lumbot" : "lumbotAI";
            var lumbotOutClass = (!this.isAI) ? "lumbotOuterRadius" : "lumbotOuterRadiusAI";

            Game.canvas.append("circle")
                .attr("class", lumbotClass)
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius);

            //Draw outer radius for debugging
            if (this.drawOuter) {
                Game.canvas.append("circle")
                    .attr("class", lumbotOutClass)
                    .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                    .attr("r", this.outterRadius);
            }

        }
    };
});