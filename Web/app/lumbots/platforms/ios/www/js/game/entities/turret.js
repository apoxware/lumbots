define("game/entities/turret", [
    'jquery',
    'd3',
    'game/entities/bullet'
], function ($, d3, Bullet) {
    return {
        id: null,
        uid: null,
        type: "turret",
        level: 1,
        hp: 20,
        radius: 10,
        outterRadius: 25,
        lat: null,
        long: null,
        drawOuter: false,
        isSelected: false,
        spawnRatio: 0, //Every 10 seconds
        energyRatio: 0, //Every 20 secs
        state: "idle",
        bullets: [],
        turnMod: 15,
        attackSpeed: 0.8,
        attCount: 0,
        isAI: false,
        target: null,
        myParentPlayer: null,
        canBeSelected: true,
        paths: [
            {"x": -10.0, "y": 5.0},
            {"x": 0.0, "y": -15.0},
            {"x": 10.0, "y": 5.0},
            {"x": 0.0, "y": 15.0}
        ],
        angle: 0,

        //spawn var
        spawn: 0,

        init: function () {
            Game.log("Init Beacon");

            //Set Beacon Properties by level
            var turretProps = this.getTurretProperties();
            turretProps.options = this.getTurretOptions();
            Game.util.appendData(this, turretProps);
            this.initAnim();
            this.myParentPlayer = (!this.isAI) ? Game.player : Game.enemy;
        },

        initAnim: function () {
            var turretClass = (!this.isAI) ? "turret" : "turretAI";
            Game.svg.append("circle")
                .attr("class", turretClass)
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius * 25)
                .style("opacity", 0)
                .transition()
                .duration(500)
                .delay(1000)
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius)
                .style("opacity", 1)
                .remove();
        },

        getTurretProperties: function () {
            var properties = $.extend(true, [], Game.entityprops[this.type].levels[this.level - 1]);
            return properties;
        },

        getTurretOptions: function () {
            var options = $.extend(true, [], Game.entityprops[this.type].options);

            //check if we can upgrade
            if (Game.entityprops[this.type].levels.length > this.level) {
                var upgradeOption =
                {
                    name: "Upgrade Turret",
                    description: "Upgrade current Turret",
                    action: "upgradeTurret",
                    requiredLvl: 0,
                    cost: this.upgradeCost()
                };
                options.push(upgradeOption);
            }
            return options;
        },

        upgradeCost: function () {
            var upgradeCost = null;
            if (Game.entityprops[this.type].levels.length > this.level) {
                upgradeCost = Game.entityprops[this.type].levels[this.level].cost;
            }
            return upgradeCost;
        },

        upgrade: function () {
            this.level++;
            this.init();
            this.upgradeAnimate();
        },

        getPoint: function () {
            return Game.map.projection([this.lat , this.long]);
        },


        generateEnergy: function (mod) {
        },


        remove: function () {


        },
        actionSelected: function (option) {
            Game.log("Action Selected and Purchased: " + option.name);
            switch (option.action) {
                case "upgradeBeacon":
                    this.upgrade();
                    this.myParentPlayer.removeEnergy(option.cost);
                    this.myParentPlayer.gainXP(Game.entityprops.xp.upgradeTurret * this.level); //Experimental. To increase
                    Game.showTimedMessage("Turret Upgraded");
                    break;
                case "deletebeacon":
                    this.remove();
                    this.myParentPlayer.removeEnergy(option.cost);
                    Game.showTimedMessage("Turret Deleted");
                    break;
                default:
                    Game.log("Unknown Action Registered");
            }
        },

        checkForTarget: function () {
            var myPosition = Game.map.projection([this.lat, this.long]);
            var myParentEnemy = (this.isAI) ? Game.player : Game.enemy;
            for (var i = 0; i < myParentEnemy.entities.length; i++) {
                var entity = myParentEnemy.entities[i];
                var targetPos = Game.map.projection([entity.lat, entity.long]);
                var isWithinOuterRange = Game.math.intercept(targetPos[0], targetPos[1], myPosition[0], myPosition[1], this.outterRadius);

                //check if is withioutter range
                if (isWithinOuterRange) {
                    this.state = "attacking";
                    this.target = entity;
                }
            }
        },

        shoot: function (target) {

            Game.log("Init Shooting Target");
            var id = this.bullets.length;
            var bullet = $.extend(true, {}, Bullet);
            bullet.id = id;
            bullet.lat = this.lat;
            bullet.long = this.long;
            bullet.target = target;
            bullet.targetPos = [target.lat, target.long];
            bullet.isAI = this.isAI;
            bullet.uid = Game.util.generateUID();
            bullet.init();
            this.bullets.push(bullet);
        },


        update: function (mod) {

            var turnSpeed = Game.math.circle / this.turnMod;
            this.checkForTarget();

            if (this.target) {
                this.attCount += this.attackSpeed * mod;
                //Attack this target
                if (this.attCount >= 1) {
                    this.shoot(this.target);
                    this.attCount = 0;
                }
                //check if target is dead and remove turret target
                if (this.target.hp) {
                    if (this.target.hp <= 0) {
                        this.target = null;
                    }
                }
            }

            //Remove extra bullets
            if (this.bullets.length > 10) {
                var bulletToDelete = this.bullets[0];
                var bulletToDeleteIndex = this.bullets.indexOf(bulletToDelete);
                this.bullets.splice(bulletToDeleteIndex, 1);
                bulletToDelete.remove();
            }

            //check if its selected
            this.drawOuter = this.isSelected;

            for (var i = 0; i < this.bullets.length; i++) {
                this.bullets[i].update(mod);
            }

        },

        draw: function () {
            var turretClass = (!this.isAI) ? "turret" : "turretAI";
            var turretAuraClass = (!this.isAI) ? "turretAura" : "turretAIAura";
            //Draw Turret
            Game.canvas.append("polygon")
                .attr("class", turretClass)
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ") rotate(" + this.angle + ")")
                .data([this.paths])
                .attr("points", function (d) {
                    return d.map(function (d) {
                        return [d.x, d.y].join(",");
                    }).join(" ");
                });
            //Draw outer radius for debugging
            if (this.drawOuter) {
                Game.canvas.append("circle")
                    .attr("class", turretAuraClass)
                    .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                    .attr("r", this.outterRadius);
            }

            for (var i = 0; i < this.bullets.length; i++) {
                this.bullets[i].draw();
            }
        }
    };
});