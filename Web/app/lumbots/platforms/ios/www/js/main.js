require.config({
    paths: {
        jquery: '../libs/jquery/dist/jquery',
        d3: '../libs/d3/d3',
        topojson: '../libs/d3/topojson',
        game: '../js/game'
    }

});

var Game = {};

require(['game/game'], function (game) {
    Game = game;

    //Get Dom Elements
    Game.ctx = document.getElementById('gameContent');

    //Bind Events functionality for n ready mobile device
    document.addEventListener('deviceready', Game.initialize, false);


    //Initialize Game
    //Game.initialize();
});