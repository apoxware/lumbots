define("game/assets/playerpos", [
    'jquery',
    'd3',
], function ($, d3) {
    return {
        id: null,
        uid: null,
        type: "bullet",
        isActive: true,
        requestLocationTimer: 5, //seconds
        location: [0, 0],
        locModCount: 0,
        radius: 15,
        options: [],
        locationRatio: 0.8,


        init: function () {
            Game.log("Init Player Position Object");
            this.setPlayerPosition();
            this.options = this.getPlayerOptions();
        },

        getPlayerPosition: function () {
            var deferred = $.Deferred();

            navigator.geolocation.getCurrentPosition($.proxy(function (position) {
                deferred.resolve([position.coords.longitude, position.coords.latitude]);

            }, this), $.proxy(function (error) {
                deferred.reject("map error loaded:" + error.message);
            }, this));

            return deferred.promise();
        },

        getPlayerOptions: function () {
            var options = $.extend(true, [], Game.entityprops["player"].options);
            return options;

        },

        actionSelected: function (option) {
            Game.log("Action Selected and Purchased: " + option.name);
            switch (option.action) {
                case "createBeacon":
                    if (Game.player.validateMaxEntity("beacon")) {
                        Game.showMessage(option.description);
                        $.when(Game.input.setEventWaiters()).then($.proxy(function () {
                                var touchLocation = Game.map.projection.invert([event.pageX , event.pageY - Game.headerHeight]);
                                if (this.isValidCreateLocation(touchLocation[0], touchLocation[1])) {
                                    Game.player.handleTransaction(option.cost);
                                    this.createBeacon(touchLocation[0], touchLocation[1]);
                                } else {
                                    Game.showMessage("This is not a valid location. Please select location within beacon aura");
                                    this.actionSelected(option);
                                }

                            }, this)
                        );
                    } else {
                        Game.showTimedMessage("You have the max amount of beacons per level. Either delete a beacon or level up.");
                    }
                default:
                    Game.log("Unknown Action Registered");
            }
        },

        createBeacon: function (lat, long) {
            Game.log("createBeacon: [" + lat + "," + long + "]");
            var newBeacon = Game.util.getModuleByType("beacon");
            newBeacon.lat = lat;
            newBeacon.long = long;
            newBeacon.level = 1;
            newBeacon.init();
            newBeacon.uid = Game.util.generateUID();
            Game.player.entities.push(newBeacon);
            Game.showTimedMessage("Beacon Created (+" + Game.entityprops.xp.createBeacon + " XP)");
            Game.player.gainXP(Game.entityprops.xp.createBeacon);
        },

        isValidCreateLocation: function (lat, long) {
            var isValid = false;
            var beaconPoint = Game.map.projection([this.location[0] , this.location[1]]);
            var actionPoint = Game.map.projection([lat , long]);


            var isWithinInnerRange = Game.math.intercept(actionPoint[0], actionPoint[1], beaconPoint[0], beaconPoint[1], this.radius);

            if (isWithinInnerRange) {
                isValid = true;
            } else {
                isValid = false
            }
            return isValid;
        },

        setPlayerPosition: function () {
            this.getPlayerPosition().done($.proxy(function (position) {
                this.location = position;
            }, this)).fail($.proxy(function (error) {
                Game.log("Error loading player position:" + error);
            }, this));
        },


        update: function (mod) {
            this.locModCount += this.locationRatio * mod;
            //Attack this target
            if (this.locModCount >= this.requestLocationTimer) {
                this.setPlayerPosition();
                this.locModCount = 0;
                this.locationAnimation();
            }

        },

        locationAnimation: function () {
            Game.svg.append("circle")
                .attr("class", "playerPosAnim")
                .attr("transform", "translate(" + Game.map.projection([this.location[0], this.location[1]]) + ")")
                .attr("r", this.radius)
                .style("opacity", 0)
                .transition()
                .style("opacity", 1)
                .transition()
                .duration(1000) // this is 1s
                .delay(100)
                .attr("r", this.radius * 5)
                .style("opacity", 0)
                .remove();
        },

        draw: function () {
            Game.canvas.append("circle")
                .attr("class", "playerPos")
                .attr("transform", "translate(" + Game.map.projection([this.location[0], this.location[1]]) + ")")
                .attr("r", this.radius);

        }
    };
});