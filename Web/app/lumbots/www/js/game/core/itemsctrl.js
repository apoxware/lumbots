define("game/core/itemsctrl", [], function () {
    return {
        items: [],
        radius: 3,

        init: function () {
            this.setEventHandlers();
        },

        setEventHandlers: function () {
            $('#avatarcontainer').click($.proxy(function () {
                this.openLumbotItemsMenu();
            }, this));
        },

        updateLumbotItemsMenu: function () {
            $("#lumbotItems").empty();


            for (var i = 0; i < Game.player.items.length; i++) {
                var item = Game.player.items[i];

                var itemDom = $('<div>', {"class": "itemImg"});

                if (item.reqLevel <= Game.player.level && !item.equipped) {
                    itemDom.click({item: item, index: i}, $.proxy(function (event) {
                        var index = event.data.index;
                        // Game.player.selectItem(selItem);
                        Game.player.selectItemAtIndex(index);
                        this.updateLumbotItemsMenu();
                    }, this));
                }

                itemDom.css("background-image", "url('" + item.path + "')");
                if (item.equipped) {
                    itemDom.css("background-color", "rgb(18, 66, 70)");
                    $("#lumbotHead").css("background-image", "url('" + item.path + "')");
                    $("#LumbotAvatarHead").css("background-image", "url('" + item.path + "')");
                }
                var itemStats = $('<div>', {"class": "itemStats"});
                itemDom.append(itemStats);

                //HP STAT
                var hpStat = $('<div>', {"class": "itemStat"});
                var itemStatSpan = $('<span>', {"html": "<b>HP:<b>"});
                var itemStatSpanVal = $('<span>', {"text": item.hp});
                hpStat.append(itemStatSpan);
                hpStat.append(itemStatSpanVal);

                //AP STAT
                var apStat = $('<div>', {"class": "itemStat"});
                itemStatSpan = $('<span>', {"html": "<b>AP:<b>"});
                itemStatSpanVal = $('<span>', {"text": item.ap});
                apStat.append(itemStatSpan);
                apStat.append(itemStatSpanVal);

                //LVL STAT
                var lvlStat = $('<div>', {"class": "itemStat"});
                itemStatSpan = $('<span>', {"html": "<b>LVL:<b>"});
                itemStatSpanVal = $('<span>', {"text": item.reqLevel});
                lvlStat.append(itemStatSpan);
                lvlStat.append(itemStatSpanVal);

                itemStats.append(hpStat);
                itemStats.append(apStat);
                itemStats.append(lvlStat);

                $("#lumbotItems").append(itemDom);

            }

            //Set Active Stats for lumbot
            $("#lumbotsActiveStats").empty();
            var currProperty = Game.util.getLumbotProperties(Game.player);
            var hpStat = $('<div>', {"class": "itemStat"});
            var itemStatSpan = $('<span>', {"html": "<b>HP:<b>"});
            var itemStatSpanVal = $('<span>', {"text": currProperty.hp});
            hpStat.append(itemStatSpan);
            hpStat.append(itemStatSpanVal);

            var apStat = $('<div>', {"class": "itemStat"});
            itemStatSpan = $('<span>', {"html": "<b>AP:<b>"});
            itemStatSpanVal = $('<span>', {"text": currProperty.ap});
            apStat.append(itemStatSpan);
            apStat.append(itemStatSpanVal);

            $("#lumbotsActiveStats").append(hpStat);
            $("#lumbotsActiveStats").append(apStat);

        },

        openLumbotItemsMenu: function () {
            $("#lumbotPopup").fadeIn();

            $("#itemStats").empty();
            this.updateLumbotItemsMenu();

            $("#lumbotSection").click(function () {
                $("#lumbotPopup").fadeOut();
            });
        },

        loadItems: function (items) {
            this.items = $.extend(true, [], items);
            $.each(this.items, function (index, value) {
                value.uid = Game.util.generateUID();
            });
        },
        getItems: function () {
            return this.items;
        },
        draw: function () {
            for (var i = 0; i < this.items.length; i++) {
                var item = this.items[i];

                Game.canvas.append("circle")
                    .attr("class", "item")
                    .attr("transform", "translate(" + Game.map.projection([item.lat, item.long]) + ")")
                    .attr("r", this.radius);
            }
        },
        update: function (mod) {

        },

        itemAquiredBy: function (item, player) {
            var itemIndex = this.items.indexOf(item);
            if (itemIndex >= 0) {
                this.items.splice(itemIndex, 1);

                //auto equip item for player
                if (player.items.length == 0 && player.level >= item.reqLevel) {
                    item.equipped = true;
                }

                this.showItemAquiredPopup(item);
                player.items.push(item);
            }

        },
        showItemAquiredPopup: function (item) {
            $("#itemPopup").fadeIn();

            var itemDom = $('<div>', {});

            //Image
            //var imagePath = item.path
            $('#itemImg').css(
                "background-image", "url('" + item.path + "')"
            );

            //Extra HP
            $('#itemDetails').empty();
            var itemStat = $('<div>', {text: "Attack Power"});
            var itemVal = $('<div>', {text: "+" + item.ap});
            itemDom.append(itemStat);
            itemDom.append(itemVal);

            //Extra AP
            itemStat = $('<div>', {text: "Health"});
            itemVal = $('<div>', {text: "+" + item.hp});
            itemDom.append(itemStat);
            itemDom.append(itemVal);

            //Extra Required Level
            itemStat = $('<div>', {text: "Required Level"});
            itemVal = $('<div>', {text: item.reqLevel});
            itemDom.append(itemStat);
            itemDom.append(itemVal);

            $('#itemDetails').append(itemDom);

            $("#closeItemButton").click(function () {
                $("#itemPopup").fadeOut();
            });
        }
    }
});