define("game/core/math", [], function () {
    return {

        circle: Math.PI * 2,

        random: function (min, max) {
            return (min + (Math.random() * (max - min)));
        },

        randomInt: function (min, max) {
            return Math.round(this.random(min, max));
        },

        randomChoice: function (choices) {
            return choices[this.randomInt(0, choices.length - 1)];
        },

        randomBool: function () {
            return this.randomChoice([true, false]);
        },

        limit: function (x, min, max) {
            return Math.max(min, Math.min(max, x));
        },

        between: function (n, min, max) {
            return ((n >= min) && (n <= max));
        },

        accelerate: function (v, accel, dt) {
            return v + (accel * dt);
        },

        lerp: function (n, dn, dt) {   // linear interpolation
            return n + (dn * dt);
        },
        overlap: function (box1, box2) {
            return !((box1.right < box2.left) ||
                (box1.left > box2.right) ||
                (box1.top > box2.bottom) ||
                (box1.bottom < box2.top));
        },
        intercept: function (x, y, xi, yi, r) {
            var dx = x - xi;
            var dy = y - yi;
            return dx * dx + dy * dy <= r * r;
        }
    };
});