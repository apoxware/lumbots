define("game/entities/beacon", [
    'jquery',
    'd3',
    'game/entities/lumbot'
], function ($, d3, Lumbot) {
    return {
        id: null,
        uid: null,
        lumbots: [],
        paths: [],
        type: "beacon",
        maxLumbots: 5,//based on level. should not be here
        level: 1,
        hp: 20,
        radius: 10,
        outterRadius: 25,
        lat: null,
        long: null,
        drawOuter: false,
        isSelected: false,
        spawnRatio: 0, //Every 10 seconds
        energyRatio: 0, //Every 20 secs
        state: "idle",
        isAI: false,
        canBeSelected: true,
        myParentPlayer: null,

        //spawn var
        spawn: 0,

        init: function () {
            Game.log("Init Beacon");

            //Set Beacon Properties by level
            var beaconProps = this.getBeaconProperties();
            beaconProps.options = this.getBeaconOptions();
            Game.util.appendData(this, beaconProps);
            this.initAnim();
            this.myParentPlayer = (!this.isAI) ? Game.player : Game.enemy;
        },

        initAnim: function () {
            var beaconClass = (!this.isAI) ? "beaconAnim" : "beaconAIAnim";
            Game.svg.append("circle")
                .attr("class", beaconClass)
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius * 25)
                .style("opacity", 0)
                .transition()
                .duration(500)
                .delay(1000)
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius)
                .style("opacity", 1)
                .remove();
        },

        getBeaconProperties: function () {
            var properties = $.extend(true, [], Game.entityprops[this.type].levels[this.level - 1]);
            return properties;
        },

        getBeaconOptions: function () {
            var options = $.extend(true, [], Game.entityprops[this.type].options);

            if (this.upgradeCost()) {
                var upgradeOption =
                {
                    name: "Upgrade Beacon",
                    description: "Upgrade current beacon",
                    action: "upgradeBeacon",
                    requiredLvl: 0,
                    cost: this.upgradeCost()
                };
                options.push(upgradeOption);
            }

            return options;
        },

        upgrade: function () {
            this.level++;
            this.init();
            this.upgradeAnimate();
        },

        upgradeCost: function () {
            var upgradeCost = null;
            if (Game.entityprops[this.type].levels.length > this.level) {
                upgradeCost = Game.entityprops[this.type].levels[this.level].cost;
            }
            return upgradeCost;
        },

        getBeaconPoint: function () {
            return Game.map.projection([this.lat , this.long]);
        },

        moveChildLumbots: function (lat, long) {
            for (var i = 0; i < this.lumbots.length; i++) {
                var lumbot = this.lumbots[i];
                lumbot.moveTo(lat, long);
            }
        },

        generateEnergy: function (mod) {

            var beaconPos = Game.map.projection([this.lat, this.long]);

            //check if all lumbots are in base
            var areInBase = false;
            for (var i = 0; i < this.lumbots.length; i++) {
                var lumbot = this.lumbots[i];
                var lumbotPos = Game.map.projection([lumbot.lat, lumbot.long]);

                var isInBase = Game.math.intercept(lumbotPos[0], lumbotPos[1], beaconPos[0], beaconPos[1], this.radius);

                if (isInBase === false) {
                    areInBase = false;
                    break;
                } else {
                    areInBase = true;
                }
            }

            //If are in base then generate energy
            if (!this.isAI && areInBase && (Game.player.energy < Game.entityprops.player.levels[Game.player.level - 1].maxEnergy)) {
                Game.player.energy += this.energyRatio * mod;
            }


        },

        spawnLumbot: function () {
            var id = this.lumbots.length;
            var lumbot = $.extend(true, {}, Lumbot);
            lumbot.id = id;
            lumbot.lat = this.lat;
            lumbot.long = this.long;
            lumbot.targetlat = this.lat;
            lumbot.isAI = this.isAI;
            lumbot.parentBeaconUId = this.uid;
            lumbot.targetlong = this.long;
            lumbot.uid = Game.util.generateUID();
            lumbot.init();
            this.lumbots.push(lumbot);
            this.myParentPlayer.entities.push(lumbot);
        },

        getId: function () {
            var index = this.myParentPlayer.entities.indexOf(this);
            return index;
        },

        remove: function () {

            var index = this.myParentPlayer.entities.indexOf(this);
            if (index > -1) {
                //Explode all lubots
                for (var l in this.lumbots) {
                    this.lumbots[l].attackAnim();
                }
                this.lumbots = [];
                // TODO: Remove unconnected paths
                this.myParentPlayer.entities.splice(index, 1);
                if (this.isAI) {
                    Game.player.gainXP(Game.entityprops.xp.destroyBeacon * this.level); //Experimental. multiply by current level of the beacon
                }
                delete this;
            }

        },


        isValidCreateLocation: function (lat, long) {
            var isValid = false;
            var beaconPoint = this.getBeaconPoint();
            var actionPoint = Game.map.projection([lat , long]);

            var isWithinOuterRange = Game.math.intercept(actionPoint[0], actionPoint[1], beaconPoint[0], beaconPoint[1], this.outterRadius);
            var isWithinInnerRange = Game.math.intercept(actionPoint[0], actionPoint[1], beaconPoint[0], beaconPoint[1], this.radius);

            if (!isWithinInnerRange && isWithinOuterRange) {
                isValid = true;
            } else {
                isValid = false
            }
            return isValid;
        },


        upgradeAnimate: function () {
            //Draw Beacon
            Game.svg.append("circle")
                .attr("class", "beaconAnim")
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius)
                .transition()
                .attr("r", this.radius * 5)
                .style("opacity", 0)
                .remove();

            Game.svg.append("circle")
                .attr("class", "beaconAnim")
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius)
                .transition()
                .attr("r", this.radius * 5)
                .style("opacity", 0)
                .remove();
        },

        createBeacon: function (lat, long) {
            Game.log("createBeacon: [" + lat + "," + long + "]");
            var newBeacon = Game.util.getModuleByType("beacon");
            newBeacon.lat = lat;
            newBeacon.long = long;
            newBeacon.level = 1;
            newBeacon.init();
            newBeacon.uid = Game.util.generateUID();
            this.myParentPlayer.entities.push(newBeacon);
            var newPath = {
                startNode: [this.lat, this.long],
                endNode: [lat, long]
            };
            this.paths.push(newPath);
            Game.showTimedMessage("Beacon Created (+" + Game.entityprops.xp.createBeacon + " XP)");
            this.myParentPlayer.gainXP(Game.entityprops.xp.createBeacon);
        },

        createTurret: function (lat, long) {
            Game.log("createTurret: [" + lat + "," + long + "]");
            var newTurret = Game.util.getModuleByType("turret");
            newTurret.lat = lat;
            newTurret.long = long;
            newTurret.level = 1;
            newTurret.init();
            newTurret.uid = Game.util.generateUID();
            this.myParentPlayer.entities.push(newTurret);
            var newPath = {
                startNode: [this.lat, this.long],
                endNode: [lat, long]
            };
            this.paths.push(newPath);
            Game.showTimedMessage("Turret Created (+" + Game.entityprops.xp.createTurret + " XP)");
            this.myParentPlayer.gainXP(Game.entityprops.xp.createTurret);
        },

        //TODO: Fix Event WAiter Actions
        actionSelected: function (option) {
            Game.log("Action Selected and Purchased: " + option.name);
            switch (option.action) {
                case "moveLumbots":
                    Game.showMessage(option.description);
                    $.when(Game.input.setEventWaiters()).then($.proxy(function () {
                            var touchLocation = Game.map.projection.invert([event.pageX , event.pageY - Game.headerHeight]);
                            if (this.isValidCreateLocation(touchLocation[0], touchLocation[1])) {
                                this.myParentPlayer.handleTransaction(option.cost);
                                this.moveChildLumbots(touchLocation[0], touchLocation[1]);
                                Game.hideMessage();
                            } else {
                                Game.showMessage("This is not a valid location. Please select location within beacon aura");
                                this.actionSelected(option);
                            }
                            Game.hideMessage();
                        }, this)
                    );
                    break;
                case "createBeacon":
                    if (this.myParentPlayer.validateMaxEntity("beacon")) {
                        Game.showMessage(option.description);
                        $.when(Game.input.setEventWaiters()).then($.proxy(function () {
                                var touchLocation = Game.map.projection.invert([event.pageX , event.pageY - Game.headerHeight]);
                                if (this.isValidCreateLocation(touchLocation[0], touchLocation[1])) {
                                    this.myParentPlayer.handleTransaction(option.cost);
                                    this.createBeacon(touchLocation[0], touchLocation[1]);
                                } else {
                                    Game.showMessage("This is not a valid location. Please select location within beacon aura");
                                    this.actionSelected(option);
                                }

                            }, this)
                        );
                    } else {
                        Game.showTimedMessage("You have the max amount of beacons per level. Either delete a beacon or level up.");
                    }

                    break;
                case "createturret":
                    var myParent = (!this.isAI) ? Game.player : Game.enemy;
                    if (this.myParentPlayer.validateMaxEntity("turret")) {
                        Game.showMessage(option.description);
                        $.when(Game.input.setEventWaiters()).then($.proxy(function () {
                                var touchLocation = Game.map.projection.invert([event.pageX , event.pageY - Game.headerHeight]);
                                if (this.isValidCreateLocation(touchLocation[0], touchLocation[1])) {
                                    this.myParentPlayer.handleTransaction(option.cost);
                                    this.createTurret(touchLocation[0], touchLocation[1]);
                                } else {
                                    Game.showMessage("This is not a valid location. Please select location within beacon aura");
                                    this.actionSelected(option);
                                }

                            }, this)
                        );
                    } else {
                        Game.showTimedMessage("You have the max amount of beacons per level. Either delete a beacon or level up.");
                    }

                    break;
                case "upgradeBeacon":
                    this.upgrade();
                    this.myParentPlayer.removeEnergy(option.cost);
                    this.myParentPlayer.gainXP(Game.entityprops.xp.upgradeBeacon * this.level); //Experimental. To increase
                    Game.showTimedMessage("Beacon Upgraded");
                    break;
                case "deletebeacon":
                    //if (Game.player.beacons.length > 1) { //TODO : FIX
                    this.remove();
                    this.myParentPlayer.removeEnergy(option.cost);
                    Game.showTimedMessage("Beacon Deleted");
                    //} else {
                    //   Game.showTimedMessage("Cannot delete this beacon.");
                    //}
                    break;
                default:
                    Game.log("Unknown Action Registered");
            }
        },


        update: function (mod) {

            //Get X and Y Position
            var position = Game.map.projection([this.lat, this.long]);

            //start spawning lumbots
            if (this.lumbots.length === 0 || this.lumbots.length < this.maxLumbots) {

                this.spawn += this.spawnRatio * mod;
                if (this.spawn >= 1) {
                    this.spawnLumbot();
                    this.spawn = 0;
                }
            }

            //generate energy
            this.generateEnergy(mod);

            //check if its selected
            this.drawOuter = this.isSelected;

            //check health
            if (this.hp <= 0) {
                this.remove();
            }

        },

        draw: function () {

            var beaconClass = (!this.isAI) ? "beacon" : "beaconAI";
            var beaconAuraClass = (!this.isAI) ? "beaconAura" : "beaconAIAura";
            var beaconOutClass = (!this.isAI) ? "beaconOuterRadius" : "beaconOuterRadiusAI";

            //Draw Beacon
            Game.canvas.append("circle")
                .attr("class", beaconClass)
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius);

            //Draw Beacon
            Game.canvas.append("circle")
                .attr("class", beaconAuraClass)
                .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                .attr("r", this.radius + 5);

            //Draw outer radius for debugging
            if (this.drawOuter) {
                Game.canvas.append("circle")
                    .attr("class", beaconOutClass)
                    .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                    .attr("r", this.outterRadius);
            }

            //Draw Paths
            for (var i = 0; i < this.paths.length; i++) {
                var path = this.paths[i];
                var startPoint = Game.map.projection([path.startNode[0], path.startNode[1]]);
                var endPoint = Game.map.projection([path.endNode[0], path.endNode[1]]);
                Game.canvas.append("line")
                    .attr("x1", startPoint[0])
                    .attr("y1", startPoint[1])
                    .attr("x2", endPoint[0])
                    .attr("y2", endPoint[1])
                    .attr("class", "path");
            }
        }
    };
});