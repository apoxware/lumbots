define("game/entities/bullet", [
    'jquery',
    'd3',
], function ($, d3) {
    return {
        id: null,
        uid: null,
        type: "bullet",
        level: 1,
        hp: 20,
        radius: 2,
        lat: null,
        long: null,
        ap: 5,
        spawnRatio: 0, //Every 10 seconds
        energyRatio: 0, //Every 20 secs
        state: "idle",
        turnMod: 15,
        isAI: false,
        target: null,
        targetPos: [],
        myParentPlayer: null,
        canBeSelected: true,
        orientation: 0,
        speed: 0.9,
        attackSpeed: 0.2,
        attCount: 0,
        isDead: false,


        init: function () {
            Game.log("Init Bullet");
            this.calculateMyOrientation();
            this.myParentPlayer = (!this.isAI) ? Game.player : Game.enemy;
            var lumbotProps = Game.util.getLumbotProperties(this.myParentPlayer);
            Game.util.appendData(this, lumbotProps);
        },

        calculateMyOrientation: function () {

            //Get Bullet Position
            var position = Game.map.projection([this.lat, this.long]);
            var posX = position[0];
            var posY = position[1];
            var turnSpeed = Game.math.circle / this.turnMod;

            //target position
            var targetPosition = Game.map.projection([this.targetPos[0], this.targetPos[1]]);
            var tarPosX = targetPosition[0];
            var tarPosY = targetPosition[1];

            var y = tarPosY - posY;
            var x = tarPosX - posX;

            //calculate orientation
            var angle = Math.atan2(y, x);
            this.orientation = angle;
        },

        update: function (mod) {

            var position = Game.map.projection([this.lat, this.long]);
            var posX = position[0];
            var posY = position[1];

            // use orientation and speed to update our position
            posX += Math.cos(this.orientation) * this.speed;
            posY += Math.sin(this.orientation) * this.speed;

            var newMapPos = Game.map.projection.invert([posX, posY]);
            this.lat = newMapPos[0];
            this.long = newMapPos[1];

            this.checkCollision(mod);

        },

        checkCollision: function (mod) {
            var myPosition = Game.map.projection([this.lat, this.long]);
            var targetPos = Game.map.projection([this.target.lat, this.target.long]);
            var isWithinInnerRange = Game.math.intercept(targetPos[0], targetPos[1], myPosition[0], myPosition[1], this.radius);
            if (isWithinInnerRange) {
                this.attack(this.target);
            }
        },

        attack: function (target) {

            target.hp -= this.ap;
            this.remove();
        },

        remove: function () {
            console.log("removing bullet");
            this.isDead = true;
            delete this;
        },

        draw: function () {
            var lumbotClass = (!this.isAI) ? "lumbot" : "lumbotAI";
            if (!this.isDead) {
                Game.canvas.append("circle")
                    .attr("class", lumbotClass)
                    .attr("transform", "translate(" + Game.map.projection([this.lat, this.long]) + ")")
                    .attr("r", this.radius);
            }
        }
    };
});