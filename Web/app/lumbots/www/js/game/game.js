// Filename: app.js
define([
    'jquery',
    'game/core/map',
    'game/core/mouse',
    'game/player',
    'game/enemy',
    'game/core/math',
    'game/core/utilities',
    'game/entities/entityprops',
    'game/core/itemsctrl',
    "d3"
], function ($, Map, Input, Player, Enemy, gameMath, Utilities, entityProps, ItemsControl, d3) {
    return {
        btnNotif: null,
        btnEnergy: null,
        txtNotif: null,
        txtEnergy: null,
        debug: true,
        authBox: null,
        ctx: null,
        svg: null,
        width: null,
        height: null,
        fps: 50,
        canvas: null,
        math: null,
        entities: null, //stores all game entities. Game loop goes thru all of them and draw and updadte
        menu: null,
        input: null, //Game input mouse and touch events

        modules: [],
        menuOpen: false,
        timedMessageMS: 5000,
        headerHeight: 53,
        playersLoadedCount: 0, //this count is used to know when all components have been loaded.

        //selectionHandlers
        // actionSelected:null,
        // selectedBeacon:null,

        //selectionObject: null,
        selectedEntity: null,

        //Delta Time Vars
        then: 0,


        initialize: function () {
            Game.log("Initialize");
            this.setViewport();
            this.load();
            this.registerDomElements();
        },

        actionUpdater: function () {
            setInterval(function () { //Maybe there is a better way to do this within update?
                Game.player.checkLevelXP();
            }, 1000.0);
        },

        load: function () {
            Game.log("Loading Game Math...");
            this.math = gameMath;
            Game.log("Loading Game Utilities...");
            this.util = Utilities;
            Game.log("Loading Game Entity Properties...");
            this.entityprops = entityProps;
            Game.log("Loading Game SVG...");
            this.loadSVG().done(function () {
                Game.loadMap().done(function () {
                    Game.loadAssets().done(function (modules) {
                        Game.loadModules(modules).done(function () {
                            Game.loadPlayer().done(function () {
                                Game.loadEnemy().done(function () {
                                    Game.loadItemsCtrl().done(function () {
                                        Game.loadGameInput();
                                        Game.hideLoading();
                                        setTimeout(Game.start, 1500);
                                    });
                                });
                            });
                        });
                    });
                });
            });
        },

        loadSVG: function () {
            var deferred = $.Deferred();
            try {
                Game.log("loadSVG");
                this.svg = d3.select("#gameContent")
                    .append("svg")
                    .attr("width", this.width)
                    .attr("height", this.height);

                this.canvas = this.svg.append("g")
                    .attr("id", "gamecanvas")
                    .attr("width", this.width)
                    .attr("height", this.height);

                deferred.resolve("SVG Loaded");
            } catch (e) {
                deferred.reject("Load SVG Error:" + error);
            }
            return deferred.promise();

        },

        loadMap: function () {

            var deferred = $.Deferred();

            navigator.geolocation.getCurrentPosition(function (position) {
                Game.log("Map Loaded");
                Game.log('Latitude: ' + position.coords.latitude + '\n' +
                    'Longitude: ' + position.coords.longitude + '\n' +
                    'Altitude: ' + position.coords.altitude + '\n' +
                    'Accuracy: ' + position.coords.accuracy + '\n' +
                    'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
                    'Heading: ' + position.coords.heading + '\n' +
                    'Speed: ' + position.coords.speed + '\n' +
                    'Timestamp: ' + position.timestamp + '\n');
                Game.map = Map;
                Game.map.center = [position.coords.longitude, position.coords.latitude];
                Game.map.init();
                Game.map.draw();
                deferred.resolve("map loaded");

            }, function (error) {
                deferred.reject("map error loaded:" + error.message);
            });

            return deferred.promise();
        },

        /**
         * Loads Assets from rest call. There can be more external assets in the future.
         * TODO: Need to change to interact with backend server instead of local file
         */
        loadAssets: function () {
            var deferred = $.Deferred();
            $.getJSON("js/game/data/assets.json", function (data) {
                Game.log("Required Assest to load returned");
                deferred.resolve(data.requires);
            });
            return deferred.promise();
        },

        loadModules: function (modules) {
            var deferred = $.Deferred();
            Game.modules = modules;
            var requireModules = Game.util.getModulePaths(modules);
            require(requireModules, $.proxy(function () {

                //Assign each require to its type
                for (var r in Game.modules) {
                    Game.modules[r].module = arguments[r];
                }
                deferred.resolve("modules loaded");
            }, this));
            return deferred.promise();
        },

        loadItemsCtrl: function () {
            var deferred = $.Deferred();
            this.itemsctrl = ItemsControl;
            $.getJSON("js/game/data/items.json", function (data) {
                Game.log("Items data returned");
                Game.itemsctrl.loadItems(data);
                Game.itemsctrl.init();
                deferred.resolve("Items loaded");
            });
            return deferred.promise();
        },

        /**
         * Load player logic and initialize its required entities.
         */
        loadPlayer: function () {
            var deferred = $.Deferred();
            this.player = Player;
            $.getJSON("js/game/data/player.json", function (data) {
                Game.log("Player data returned");
                Game.util.appendData(Game.player, data);
                Game.player.initEntities(Game.playersLoaded);
                Game.player.init();
                deferred.resolve("player loaded");
            });
            return deferred.promise();
        },

        /**
         * Load Enemy AI logic and initialize its required entities.
         */
        loadEnemy: function () {
            var deferred = $.Deferred();
            this.enemy = Enemy;
            $.getJSON("js/game/data/enemy.json", function (data) {
                Game.log("Enemy data returned");
                Game.util.appendData(Game.enemy, data);
                Game.enemy.initEntities(Game.playersLoaded);
                Game.enemy.init();
                deferred.resolve("enemy loaded");
            });
            return deferred.promise();
        },

        /**
         * Once both players have been loaded we initialize the game
         * TODO: In future we would check multiplayer players are also loaded
         */
//        playersLoaded: function () {
//            Game.playersLoadedCount++;
//            if (Game.playersLoadedCount === 2) {
//                setTimeout(Game.start, 1500);
//            }
//        },

        loadGameInput: function () {
            Game.log("loadInput");
            Game.input = Input;
            Game.input.init();
        },

        /**
         *Main Game loop is started here
         */
        start: function () {
            Game.then = Date.now();
            Game.actionUpdater();
            var animFrame = window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                null;

            if (animFrame !== null) {
                var recursiveAnim = function () {
                    Game.loop();
                    animFrame(recursiveAnim);
                };

                // start the main loop
                animFrame(recursiveAnim);
            } else {
                setInterval(Game.loop, 1000.0 / Game.fps);
            }
        },

        timestamp: function () {
            return new Date().getTime();
        },

        loop: function () {
            //Set delta
            var now = Date.now();
            var delta = now - this.then;

            //Draw and update
            Game.draw();
            Game.update(delta / 1000);

            //Set then to now
            this.then = now;
        },

        update: function (mod) {

            //update player objects
            for (var i = 0; i < this.player.entities.length; i++) {
                this.player.entities[i].update(mod);
            }

            //update enemy objects
            for (var i = 0; i < this.enemy.entities.length; i++) {
                this.enemy.entities[i].update(mod);
            }

            //Update Items
            Game.itemsctrl.update(mod);

            //Update player Position
            this.player.position.update(mod);

        },

        draw: function () {

            //delete all game components
            this.canvas.selectAll("*").remove();

            //draw player objects
            for (var i = 0; i < this.player.entities.length; i++) {
                this.player.entities[i].draw();
            }

            //draw enemy objects
            for (var i = 0; i < this.enemy.entities.length; i++) {
                this.enemy.entities[i].draw();
            }

            //draw items
            //TODO: Fix update of options
            Game.itemsctrl.draw();

            //Draw Player Info
            this.drawStats();

            //Draw player location
            this.player.position.draw();
        },


        checkObjectSelection: function (lat, long) {
            var selectionPoint = Game.map.projection([lat , long]);

            //look for selection in player pool
            for (var k in this.player.entities) {
                var playerEntity = this.player.entities[k];
                if (!playerEntity.canBeSelected) {
                    continue;
                }
                var playerEntityPoint = Game.map.projection([playerEntity.lat, playerEntity.long]);
                var playerEntityRadius = playerEntity.radius;
                var playerEntityIsSelected = Game.math.intercept(selectionPoint[0], selectionPoint[1], playerEntityPoint[0], playerEntityPoint[1], playerEntityRadius);
                playerEntity.isSelected = playerEntityIsSelected;
                if (playerEntity.isSelected) {
                    Game.log("isSelected: " + playerEntity);
                    this.selectedEntity = playerEntity;
                }
            }
            var playerPositionPoint = Game.map.projection([Game.player.position.location[0], Game.player.position.location[1]]);
            var playerPositionObjectSelected = Game.math.intercept(selectionPoint[0], selectionPoint[1], playerPositionPoint[0], playerPositionPoint[1], Game.player.position.radius);
            Game.player.position.isSelected = playerPositionObjectSelected;

            if (!this.selectedEntity) {
                //check if selectionPoint is on player object
                if (Game.player.position.isSelected) {
                    this.selectedEntity = Game.player.position;
                } else {
                    return;
                }

            }

            //Check if we have a selection
            if (this.selectedEntity.isSelected === false) {
                this.selectedEntity = null;
                Game.util.hideEntityDisplay();
                return;
            }

            //Check if selectedEntity has Options.Else retrun
            if (!this.selectedEntity.options) {
                return;
            }
            //Check if selectedEntity has Options. Load them
            if (this.selectedEntity.options.length > 0) {
                Game.util.loadOptionsForEntity(this.selectedEntity);
            } else {
                $('#actionslist').fadeOut(1000, function () {
                    $('#actionslist').empty();
                    $('#actionslist').fadeIn();
                });
                $('#avatarstats').fadeOut(1000, function () {
                    $('#avatarstats').empty();
                    $('#avatarstats').fadeIn();
                });
            }
        },
        validateActionPurchase: function (actionCost) {
            var hasEnergy = false;
            if (Game.player.energy >= actionCost) {
                hasEnergy = true;
            } else {
                hasEnergy = false;
                Game.showTimedMessage("Not enough energy to generate action");
            }
            return hasEnergy;
        },
        /*
         * Once a menu option is clicked we intercept the call to the game controller to validate the transaction with
         * the user energy and action cost
         * */
        menuActionClicked: function (event) {
            var entity = event.data.entity;
            var option = event.data.option;
            var actionCost = option.cost;
            Game.log("ActonClicked: " + option.name);
            //check user has enough energy to buy
            if (Game.validateActionPurchase(actionCost)) {
                Game.log("User has enough energy to purchase action");
                entity.actionSelected(option);
            }
        },

        showTimedMessage: function (msg) {
            this.showMessage(msg);
            setTimeout(function () {
                Game.hideMessage()
            }, this.timedMessageMS);
        },

        showMessage: function (msg) {
            $("#message").fadeIn();
            $("#message").html(msg);
        },
        hideMessage: function () {
            $("#message").fadeOut();
        },

        showLoading: function () {
            $("#loading").fadeIn();
        },
        hideLoading: function () {
            $("#loading").fadeOut();
        },


        drawStats: function () {
            var stringlength = 3;

            $("#energyText").html(parseInt(this.player.energy.toString().substring(0, stringlength)));
            $("#NotificationsText").html(this.player.level.toString().substring(0, stringlength));

            if (this.selectedEntity) {
                this.updateMenuStats();
            }

            //Draw progress bar
            var percent = this.player.levelXP / this.player.levelCap * 100;
            $("#NotificationsProgress").css("width", percent + "%");
        },


        updateMenuStats: function () {

            var entity = this.selectedEntity;

            //Get Strings
            var stringlength = 5;
            var entityHP = entity.hp ? entity.hp : null;
            var entityLumLength = (entity.lumbots) ? entity.lumbots.length : null;
            var entityLumMax = (entity.lumbots) ? entity.maxLumbots : null;
            var entityLvl = entity.level ? entity.level : null;
            var entityLat = entity.lat ? entity.lat : null;
            var entityLong = entity.long ? entity.long : null;
            var entitySpawn = entity.spawnRatio;

            //Add Stats
            if (entityHP && entityLvl && entityLat && entityLong) {
                $('#avatarstats').empty();
                $("#avatarstats").append("<div>HP: " + entityHP.toString().substring(0, stringlength) + "</div>");
                $("#avatarstats").append("<div>level: " + entityLvl.toString().substring(0, stringlength) + "</div>");
                $("#avatarstats").append("<div>Lat: " + entityLat.toString().substring(0, stringlength) + "</div>");
                $("#avatarstats").append("<div>Long: " + entityLong.toString().substring(0, stringlength) + "</div>");
            }

            //Conditional entity properties
            if (entityLumLength) {
                $("#avatarstats").append("<div>lumbots: " + entityLumLength + " / " + entityLumMax + "</div>");
            }

            if (entitySpawn) {
                $("#avatarstats").append("<div>spawnRatio: " + entitySpawn.toString().substring(0, stringlength) + "</div>");
            }

            Game.util.updateOptionsForEntity(entity);

        },

        renderPreview: function (item) {

            $("#avatarimage").empty();

            var svg = d3.select("#avatarimage").append("svg")
                .attr("width", "100%")
                .attr("height", "100%");

            var height = $("#avatarimage").height();
            var width = $("#avatarimage").width();
            var radius = height / 3;
            var xpos = width / 2;
            var ypos = height / 3;


            //Draw Beacon
            svg.append("circle")
                .attr("class", "beacon")
                .attr("transform", "translate(" + [xpos, ypos] + ")")
                .attr("r", radius);

            //draw lumbots?


        },

        setViewport: function () {
            Game.log("setViewPort");
            this.height = $(this.ctx).height();
            this.width = $(this.ctx).width();
            $(window).resize(Game.resize);
        },

        resize: function (event) {
            Game.log("Resize Triggered");
        },

        registerDomElements: function () {
            Game.log("registerDomElements");
            this.btnNotif = $('#nbutton');
            this.btnEnergy = $('#ebutton');
            this.txtNotif = $('#energyText');
            this.txtEnergy = $('#NotificationsText');
            this.authBox = $('#authBox');
            this.menu = $("#rightMenu");
        },

        resizeCanvas: function () {
            var tmpHeight = $(window).height() - 131;
            var tmpWidth = $(window).width();

            canvas.width = tmpWidth;
            canvas.height = tmpHeight;
        },

        promptAuthentication: function () {
            Game.log("promptAuthentication");
            this.authBox.fadeIn("slow", $.proxy(function () {
                Game.log("authBox Displayed");
            }, this));

        },

        log: function (str) {
            if (this.debug) {
                console.log(str);
            }
        }
    };
});