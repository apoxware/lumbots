define("game/player", [
    'jquery',
    "d3",
], function ($, d3) {
    return {
        level: 1,
        id: null,
        clazz: "player",
        maxEnergy: 0,
        maxBeacons: 0,
        levelXP: 0,
        levelCap: 0,
        entities: [],
        requires: [],
        energy: 0,
        position: null,


        init: function () {
            Game.log("Game Player Init");

            //Set Beacon Properties by level
            var playerProps = Game.entityprops[this.clazz].levels[this.level - 1];
            Game.util.appendData(this, playerProps);
            this.initPlayerPosition();

        },

        gainXP: function (xp) {
            Game.util.showGainXPAnimation(xp);
            this.levelXP += xp;
        },

        gainEnergy: function (energyAmt) {
            this.energy += energyAmt;
        },

        removeEnergy: function (energyAmt) {
            this.energy -= energyAmt;
        },

        handleTransaction: function (cost) {
            this.energy -= cost;
        },

        selectItem: function (item) {

            var itemIndex = this.items.indexOf(item);
            for (var i = 0; i < this.items.length; i++) {
                this.items[i].equipped = false;
            }
            this.items[itemIndex].equipped = true;
        },

        selectItemAtIndex: function (index) {

            //var itemIndex = this.items.indexOf(item);
            for (var i = 0; i < this.items.length; i++) {
                this.items[i].equipped = false;
            }
            this.items[index].equipped = true;
        },

        validateMaxEntity: function (type) {
            var isValid = false;
            var playerProps = Game.entityprops["player"].levels[this.level - 1];
            var playerMaxEntities = playerProps[type];
            var currentEntitiesLength = Game.util.getAllEntitiesByType(Game.player, type).length;
            if (currentEntitiesLength < playerMaxEntities) {
                isValid = true;
            }
            return isValid;
        },

        initEntities: function () {
            var entities = $.extend(true, [], this.entities);
            this.entities = [];
            //Init each entity with corresponding type
            for (var e in entities) {
                var entity = entities[e];
                var entityModule = Game.util.getModuleByType(entity.type);
                Game.util.appendData(entityModule, entity);
                entityModule.isAI = false;
                entityModule.uid = Game.util.generateUID();
                this.entities.push(entityModule);
                entityModule.init();
            }
        },

        initPlayerPosition: function () {
            var playerPositionModule = Game.util.getModuleByType("playerpos");
            this.position = playerPositionModule;
            this.position.init();
        },

        levelUp: function () {
            this.level++;
            this.init();
            Game.showTimedMessage("Congratulations! You have leveled up!");
            //Game.showTimedMessage("Player has gained up!");
        },
        checkLevelXP: function () {
            if (this.levelXP >= this.levelCap) {
                this.levelUp();
            }
        }

    };
});