require.config({
    paths: {
        jquery: '../libs/jquery/dist/jquery',
        d3: '../libs/d3/d3',
        topojson: '../libs/d3/topojson',
        game: '../js/game'
    }

});

var Game = {};
require(['game/game'], function (game) {
    Game = game;

    //Get Dom Elements
    Game.ctx = document.getElementById('gameContent');

    //Load game after device is ready
//    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
//        document.addEventListener("deviceready", Game.initialize, false);
//    } else {
//        Game.initialize();
//    }


    Game.initialize();
});