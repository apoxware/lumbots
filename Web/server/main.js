var express = require('express');
var app = express();

//SET WEB CONTENT
app.use("/", express.static('../app/lumbots/www'));


//REST REQUESTS
app.get('/controller', function (req, res) {
    res.send('LumbotController');
});

app.listen(8181);
console.log("Lumbots Backend Running on Port : 8181");